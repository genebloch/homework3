﻿using BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("UserTasksInProject/{id}")]
        public IActionResult GetUserTasksInProject(int id)
        {
            var output = _linqService.GetUserTasksInProject(id);

            if (output == null) return NotFound(output);

            return Ok((output.Values, output.Keys));
        }

        [HttpGet("UserTasksWithRestrictedNameLength/{id}")]
        public IActionResult GetUserTasksWithRestrictedNameLength(int id)
        {
            var tasks = _linqService.GetUserTasksWithRestrictedNameLength(id);

            if (tasks == null) return NotFound(tasks);

            return Ok(tasks);
        }

        [HttpGet("UserFinishedTasksIn2020/{id}")]
        public IActionResult GetUserFinishedTasksIn2020(int id)
        {
            var output = _linqService.GetUserFinishedTasksIn2020(id);

            if (output == null) return NotFound(output);

            return Ok(output);
        }

        [HttpGet("UsersOlderThan10Years")]
        public IActionResult GetUsersOlderThan10Years()
        {
            var users = _linqService.GetUsersOlderThan10Years();

            return Ok(users);
        }

        [HttpGet("SortedUsersWithTasks")]
        public IActionResult GetSortedUsersWithTasks()
        {
            var output = _linqService.GetSortedUsersWithTasks();

            return Ok(output);
        }

        [HttpGet("UserInfo/{id}")]
        public IActionResult GetUserInfo(int id)
        {
            var info = _linqService.GetUserInfo(id);

            if (info == null) return NotFound(info);

            return Ok(info);
        }

        [HttpGet("ProjectsInfo")]
        public IActionResult GetProjectsInfo()
        {
            var info = _linqService.GetProjectsInfo();

            return Ok(info);
        }
    }
}
