﻿using BLL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projecsService;

        public ProjectsController(IService<ProjectDTO> projecsService)
        {
            _projecsService = projecsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var projects =  _projecsService.GetAll();

            return Ok(projects);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var project = _projecsService.Get(id);

            if (project == null) return NotFound(project);

            return Ok(project);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            _projecsService.Create(project);

            return Created($"{project.Id}", project);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            var updated = _projecsService.Update(project);

            if (!updated) return NotFound(project);

            return Ok(project);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var deleted = _projecsService.Delete(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
