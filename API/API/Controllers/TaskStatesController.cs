﻿using BLL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly IService<TaskStateDTO> _statesService;

        public TaskStatesController(IService<TaskStateDTO> statesService)
        {
            _statesService = statesService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var states = _statesService.GetAll();

            return Ok(states);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var state = _statesService.Get(id);

            if (state == null) return NotFound(state);

            return Ok(state);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskStateDTO state)
        {
            _statesService.Create(state);

            return Created($"{state.Id}", state);
        }

        [HttpPut]
        public IActionResult Update([FromBody] TaskStateDTO state)
        {
            var updated = _statesService.Update(state);

            if (!updated) return NotFound(state);

            return Ok(state);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var deleted = _statesService.Delete(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
