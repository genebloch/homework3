﻿using BLL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _usersService;

        public UsersController(IService<UserDTO> usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _usersService.GetAll();

            return Ok(users);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = _usersService.Get(id);

            if (user == null) return NotFound(user);

            return Ok(user);
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserDTO user)
        {
            _usersService.Create(user);

            return Created($"{user.Id}", user);
        }

        [HttpPut]
        public IActionResult Update([FromBody] UserDTO user)
        {
            var updated = _usersService.Update(user);

            if (!updated) return NotFound(user);

            return Ok(user);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var deleted = _usersService.Delete(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}
