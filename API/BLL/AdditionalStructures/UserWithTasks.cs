﻿using DAL.Models;
using System.Collections.Generic;

namespace BLL.AdditionalStructures
{
    public class UserWithTasks
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
