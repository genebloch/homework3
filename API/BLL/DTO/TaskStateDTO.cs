﻿namespace DAL.Models
{
    public class TaskStateDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
