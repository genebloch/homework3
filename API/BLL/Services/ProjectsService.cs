﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;

namespace BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(ProjectDTO item) => _unitOfWork.Projects.Create(_mapper.Map<Project>(item));
        public bool Delete(int id) => _unitOfWork.Projects.Delete(id);

        public ProjectDTO Get(int id)
        {
            var project = _unitOfWork.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            var projects = _unitOfWork.Projects.GetAll();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public bool Update(ProjectDTO item) => _unitOfWork.Projects.Update(_mapper.Map<Project>(item));
    }
}
