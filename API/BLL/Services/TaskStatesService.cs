﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;

namespace BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskStatesService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TaskStateDTO item) => _unitOfWork.States.Create(_mapper.Map<TaskState>(item));
        public bool Delete(int id) => _unitOfWork.States.Delete(id);

        public TaskStateDTO Get(int id)
        {
            var state = _unitOfWork.States.Get(id);

            return _mapper.Map<TaskStateDTO>(state);
        }

        public IEnumerable<TaskStateDTO> GetAll()
        {
            var states = _unitOfWork.States.GetAll();

            return _mapper.Map<IEnumerable<TaskStateDTO>>(states);
        }

        public bool Update(TaskStateDTO item) => _unitOfWork.States.Update(_mapper.Map<TaskState>(item));
    }
}
