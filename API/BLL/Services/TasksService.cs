﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;

namespace BLL.Services
{
    public class TasksService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TaskDTO item) => _unitOfWork.Tasks.Create(_mapper.Map<Task>(item));
        public bool Delete(int id) => _unitOfWork.Tasks.Delete(id);

        public TaskDTO Get(int id)
        {
            var task = _unitOfWork.Tasks.Get(id);

            return _mapper.Map<TaskDTO>(task);
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            var tasks = _unitOfWork.Tasks.GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public bool Update(TaskDTO item) => _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
    }
}
