﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;

namespace BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TeamDTO item) => _unitOfWork.Teams.Create(_mapper.Map<Team>(item));
        public bool Delete(int id) => _unitOfWork.Teams.Delete(id);

        public TeamDTO Get(int id)
        {
            var team = _unitOfWork.Teams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            var teams = _unitOfWork.Teams.GetAll();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public bool Update(TeamDTO item) => _unitOfWork.Teams.Update(_mapper.Map<Team>(item));
    }
}
