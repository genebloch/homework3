﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;

namespace BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(UserDTO item) => _unitOfWork.Users.Create(_mapper.Map<User>(item));
        public bool Delete(int id) => _unitOfWork.Users.Delete(id);

        public UserDTO Get(int id)
        {
            var user = _unitOfWork.Users.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var users = _unitOfWork.Users.GetAll();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public bool Update(UserDTO item) => _unitOfWork.Users.Update(_mapper.Map<User>(item));
    }
}
