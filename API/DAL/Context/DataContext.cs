﻿using DAL.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DAL.Context
{
    public class DataContext
    {
        public List<User> Users { get; private set; }
        public List<Team> Teams { get; private set; }
        public List<Project> Projects { get; private set; }
        public List<Task> Tasks { get; private set; }
        public List<TaskState> States { get; private set; }

        public DataContext()
        {
            Users = Get<User>("Users");
            Teams = Get<Team>("Teams");
            Projects = Get<Project>("Projects");
            Tasks = Get<Task>("Tasks");
            States = Get<TaskState>("TaskStates");
        }

        private List<T> Get<T>(string model)
        {
            List<T> output = null;

            using (StreamReader reader = new StreamReader($"../db/{model}.json"))
            {
                string json = reader.ReadToEnd();

                output = JsonConvert.DeserializeObject<List<T>>(json);
            }

            return output;
        }
    }
}
