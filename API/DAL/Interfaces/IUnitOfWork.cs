﻿using DAL.Repositories;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ProjectsRepository Projects { get; }
        TasksRepository Tasks { get; }
        TaskStatesRepository States { get; }
        TeamsRepository Teams { get; }
        UsersRepository Users { get; }
    }
}
