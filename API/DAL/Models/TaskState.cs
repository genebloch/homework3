﻿namespace DAL.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
