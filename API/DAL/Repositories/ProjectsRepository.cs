﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class ProjectsRepository : IRepository<Project>
    {
        private readonly DataContext _context;

        public ProjectsRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(Project item)
        {
            item.Id = GetIdForNewEntity();

            _context.Projects.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _context.Projects.FirstOrDefault(p => p.Id == id);

            return _context.Projects.Remove(item);
        }

        public Project Get(int id) => _context.Projects.FirstOrDefault(u => u.Id == id);
        public IEnumerable<Project> GetAll() => _context.Projects;

        public bool Update(Project item)
        {
            var value = _context.Projects.FirstOrDefault(u => u.Id == item.Id);

            if (value == null) return false;

            var index = _context.Projects.IndexOf(value);
            _context.Projects[index] = item;

            return true;
        }

        private int GetIdForNewEntity() => _context.Projects.Max(p => p.Id) + 1;
    }
}
