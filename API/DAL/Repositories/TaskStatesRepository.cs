﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class TaskStatesRepository : IRepository<TaskState>
    {
        private readonly DataContext _context;

        public TaskStatesRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(TaskState item)
        {
            item.Id = GetIdForNewEntity();

            _context.States.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _context.States.FirstOrDefault(s => s.Id == id);

            return _context.States.Remove(item);
        }

        public TaskState Get(int id) => _context.States.FirstOrDefault(s => s.Id == id);
        public IEnumerable<TaskState> GetAll() => _context.States;

        public bool Update(TaskState item)
        {
            var value = _context.States.FirstOrDefault(s => s.Id == item.Id);

            if (value == null) return false;

            var index = _context.States.IndexOf(value);
            _context.States[index] = item;

            return true;
        }

        private int GetIdForNewEntity() => _context.States.Max(s => s.Id) + 1;
    }
}
