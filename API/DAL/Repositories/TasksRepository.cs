﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class TasksRepository : IRepository<Task>
    {
        private readonly DataContext _context;

        public TasksRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(Task item)
        {
            item.Id = GetIdForNewEntity();

            _context.Tasks.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _context.Tasks.FirstOrDefault(t => t.Id == id);

            return _context.Tasks.Remove(item);
        }

        public Task Get(int id) => _context.Tasks.FirstOrDefault(t => t.Id == id);
        public IEnumerable<Task> GetAll() => _context.Tasks;

        public bool Update(Task item)
        {
            var value = _context.Tasks.FirstOrDefault(t => t.Id == item.Id);

            if (value == null) return false;

            var index = _context.Tasks.IndexOf(value);
            _context.Tasks[index] = item;

            return true;
        }

        private int GetIdForNewEntity() => _context.Tasks.Max(t => t.Id) + 1;
    }
}
