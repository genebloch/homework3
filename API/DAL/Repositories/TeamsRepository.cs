﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class TeamsRepository : IRepository<Team>
    {
        private readonly DataContext _context;

        public TeamsRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(Team item)
        {
            item.Id = GetIdForNewEntity();

            _context.Teams.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _context.Teams.FirstOrDefault(t => t.Id == id);

            return _context.Teams.Remove(item);
        }

        public Team Get(int id) => _context.Teams.FirstOrDefault(t => t.Id == id);
        public IEnumerable<Team> GetAll() => _context.Teams;

        public bool Update(Team item)
        {
            var value = _context.Teams.FirstOrDefault(t => t.Id == item.Id);

            if (value == null) return false;

            var index = _context.Teams.IndexOf(value);
            _context.Teams[index] = item;

            return true;
        }

        private int GetIdForNewEntity() => _context.Teams.Max(t => t.Id) + 1;
    }
}
