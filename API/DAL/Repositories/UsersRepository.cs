﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private readonly DataContext _context;

        public UsersRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(User item)
        {
            item.Id = GetIdForNewEntity();

            _context.Users.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _context.Users.FirstOrDefault(u => u.Id == id);

            return _context.Users.Remove(item);
        }

        public User Get(int id) => _context.Users.FirstOrDefault(u => u.Id == id);
        public IEnumerable<User> GetAll() => _context.Users;

        public bool Update(User item)
        {
            var value = _context.Users.FirstOrDefault(u => u.Id == item.Id);

            if (value == null) return false;

            var index = _context.Users.IndexOf(value);
            _context.Users[index] = item;

            return true;
        }

        private int GetIdForNewEntity() => _context.Users.Max(u => u.Id) + 1;
    }
}
