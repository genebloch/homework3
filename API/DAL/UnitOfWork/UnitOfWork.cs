﻿using DAL.Context;
using DAL.Interfaces;
using DAL.Repositories;

namespace DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        private ProjectsRepository _projectsRepository;
        private TasksRepository _tasksRepository;
        private TaskStatesRepository _statesRepository;
        private TeamsRepository _teamsRepository;
        private UsersRepository _usersRepository;

        public UnitOfWork()
        {
            _context = new DataContext();
        }

        public ProjectsRepository Projects
        {
            get
            {
                if (_projectsRepository == null) _projectsRepository = new ProjectsRepository(_context);

                return _projectsRepository;
            }
        }

        public TasksRepository Tasks
        {
            get
            {
                if (_tasksRepository == null) _tasksRepository = new TasksRepository(_context);

                return _tasksRepository;
            }
        }

        public TaskStatesRepository States
        {
            get
            {
                if (_statesRepository == null)  _statesRepository = new TaskStatesRepository(_context);
                
                return _statesRepository;
            }
        }

        public TeamsRepository Teams
        {
            get
            {
                if (_teamsRepository == null) _teamsRepository = new TeamsRepository(_context);

                return _teamsRepository;
            }
        }

        public UsersRepository Users
        {
            get
            {
                if (_usersRepository == null) _usersRepository = new UsersRepository(_context);
                
                return _usersRepository;
            }
        }
    }
}
