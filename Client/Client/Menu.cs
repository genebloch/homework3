﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Menu
    {
        private static string _base;
        private static HttpClient _client;

        public Menu()
        {
            _base = "https://localhost:44354/api";
            _client = new HttpClient();
        }

        public async Task RunAsync() => await ShowAvailableEntitiesAsync();

        private async Task ShowAvailableEntitiesAsync()
        {
            Console.WriteLine("1: Projects");
            Console.WriteLine("2: Tasks");
            Console.WriteLine("3: TaskStates");
            Console.WriteLine("4: Teams");
            Console.WriteLine("5: Users");
            Console.WriteLine("6: LINQ");

            await ChooseEnitityAsync(ReceiveCommand());
        }

        private async Task ShowAvailableOperationsAsync(string entity)
        {
            Console.WriteLine("1: GetAll");
            Console.WriteLine("2: Get");
            Console.WriteLine("3: Delete");
            Console.WriteLine("4: Create");
            Console.WriteLine("5: Update");

            await ChooseOperationAsync(entity, ReceiveCommand());
        }

        private int ReceiveCommand()
        {
            Console.Write("> ");

            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch (Exception)
            {
                return -1;
            }
        }

        private async Task ChooseEnitityAsync(int command)
        {
            switch (command)
            {
                case 1:
                    await ShowAvailableOperationsAsync("Projects");
                    break;
                case 2:
                    await ShowAvailableOperationsAsync("Tasks");
                    break;
                case 3:
                    await ShowAvailableOperationsAsync("TaskStates");
                    break;
                case 4:
                    await ShowAvailableOperationsAsync("Teams");
                    break;
                case 5:
                    await ShowAvailableOperationsAsync("Users");
                    break;
                case 6:
                    await ShowAvailableLinqQueries();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Invalid command");
                    await ShowAvailableEntitiesAsync();
                    break;
            }
        }

        private async Task GoBackAsync()
        {
            Console.WriteLine("Press any button to go back");
            Console.ReadKey();
            Console.Clear();
            await ShowAvailableEntitiesAsync();
        }

        private async Task ShowAvailableLinqQueries()
        {
            Console.WriteLine("1: Get user tasks in project");
            Console.WriteLine("2: Get user tasks with restricted name length");
            Console.WriteLine("3: Get user finished tasks in 2020");
            Console.WriteLine("4: Get users older than 10 years");
            Console.WriteLine("5: Get sorted users with tasks");
            Console.WriteLine("6: Get user info");
            Console.WriteLine("7: Get projects info");

            await ChooseLinqQuery(ReceiveCommand());
        }

        private async Task ChooseLinqQuery(int command)
        {
            switch (command)
            {
                case 1:
                    {
                        Console.Write("Id: ");
                        string id = Console.ReadLine();

                        try
                        {
                            await GetLinqResponse("UserTasksInProject", int.Parse(id));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        await GoBackAsync();
                        break;
                    }
                case 2:
                    {
                        Console.Write("Id: ");
                        string id = Console.ReadLine();

                        try
                        {
                            await GetLinqResponse("UserTasksWithRestrictedNameLength", int.Parse(id));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        await GoBackAsync();
                        break;
                    }
                case 3:
                    {
                        Console.Write("Id: ");
                        string id = Console.ReadLine();

                        try
                        {
                            await GetLinqResponse("UserFinishedTasksIn2020", int.Parse(id));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        await GoBackAsync();
                        break;
                    }
                case 4:
                    {
                        await GetLinqResponse("UsersOlderThan10Years");
                        await GoBackAsync();
                        break;
                    }
                case 5:
                    {
                        await GetLinqResponse("SortedUsersWithTasks");
                        await GoBackAsync();
                        break;
                    }
                case 6:
                    {
                        Console.Write("Id: ");
                        string id = Console.ReadLine();

                        try
                        {
                            await GetLinqResponse("UserInfo", int.Parse(id));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        await GoBackAsync();
                        break;
                    }
                case 7:
                    {
                        await GetLinqResponse("ProjectsInfo");
                        await GoBackAsync();
                        break;
                    }
            }
        }

        private async Task GetLinqResponse(string request, params int[] id)
        {
            HttpResponseMessage response;

            if (id.Length != 0) response = await _client.GetAsync($"{_base}/Linq/{request}/{id[0]}");
            else response = await _client.GetAsync($"{_base}/Linq/{request}");

            response.EnsureSuccessStatusCode();
            var output = await response.Content.ReadAsStringAsync();

            Console.WriteLine(output);
        }

        private async Task ChooseOperationAsync(string entity, int command)
        {
            switch (command)
            {
                case 1:
                    await GetAll(entity);
                    await GoBackAsync();
                    break;
                case 2:
                    await Get(entity);
                    await GoBackAsync();
                    break;
                case 3:
                    await Delete(entity);
                    await GoBackAsync();
                    break;
                case 4:
                    await Create(entity);
                    await GoBackAsync();
                    break;
                case 5:
                    await Update(entity);
                    await GoBackAsync();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Invalid command");
                    await ShowAvailableEntitiesAsync();
                    break;
            }
        }

        private async Task Create(string entity)
        {
            StringContent content = null;

            switch (entity)
            {
                case "Projects":
                    content = GetContent(CreateProject());
                    break;
                case "Tasks":
                    content = GetContent(CreateTask());
                    break;
                case "Teams":
                    content = GetContent(CreateTeam());
                    break;
                case "Users":
                    content = GetContent(CreateUser());
                    break;
                case "TaskStates":
                    content = GetContent(CreateTaskState());
                    break;
            }

            if (content != null) await _client.PostAsync($"{_base}/{entity}", content);
        }

        private async Task GetAll(string entity)
        {
            HttpResponseMessage response = await _client.GetAsync($"{_base}/{entity}");
            response.EnsureSuccessStatusCode();
            var output = await response.Content.ReadAsStringAsync();

            Console.WriteLine(output);
        }

        private async Task Get(string entity)
        {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            try
            {
                HttpResponseMessage response = await _client.GetAsync($"{_base}/{entity}/{int.Parse(id)}");
                response.EnsureSuccessStatusCode();
                var output = await response.Content.ReadAsStringAsync();

                Console.WriteLine(output);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task Delete(string entity)
        {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            try
            {
                HttpResponseMessage response = await _client.DeleteAsync($"{_base}/{entity}/{id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task Update(string entity)
        {
            Console.Write("Id: ");

            try
            {
                int id = int.Parse(Console.ReadLine());

                StringContent content = null;

                switch (entity)
                {
                    case "Projects":
                        content = GetContent(CreateProject(id));
                        break;
                    case "Tasks":
                        content = GetContent(CreateTask(id));
                        break;
                    case "Teams":
                        content = GetContent(CreateTeam(id));
                        break;
                    case "Users":
                        content = GetContent(CreateUser(id));
                        break;
                    case "TaskStates":
                        content = GetContent(CreateTaskState(id));
                        break;
                }

                if (content != null) await _client.PutAsync($"{_base}/{entity}", content);
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("Invalid command");
                await ShowAvailableEntitiesAsync();
            }
        }

        private StringContent GetContent<T>(T model)
        {
            string json = JsonConvert.SerializeObject(model);

            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private Models.Project CreateProject(params int[] id)
        {
            Models.Project project = null;

            int itemId = 0;

            if (id.Length != 0) itemId = id[0];

            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Description: ");
            string description = Console.ReadLine();

            try
            {
                Console.Write("Team id: ");
                int teamId = int.Parse(Console.ReadLine());

                Console.Write("Author id: ");
                int authorId = int.Parse(Console.ReadLine());

                Console.Write("Created at (e.g. dd/mm/yyyy): ");
                DateTime createdAt = DateTime.Parse(Console.ReadLine());

                Console.Write("Deadline (e.g. dd/mm/yyyy): ");
                DateTime deadline = DateTime.Parse(Console.ReadLine());

                project = new Models.Project()
                { 
                    Id = itemId, Description = description, Name = name, TeamId = teamId, 
                    AuthorId = authorId, CreatedAt = createdAt, Deadline = deadline
                };
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }

            return project;
        }

        private Models.Task CreateTask(params int[] id)
        {
            Models.Task task = null;
            
            int itemId = 0;

            if (id.Length != 0) itemId = id[0];

            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Description: ");
            string description = Console.ReadLine();

            try
            {
                Console.Write("Project id: ");
                int projectId = int.Parse(Console.ReadLine());

                Console.Write("Performer id: ");
                int performerId = int.Parse(Console.ReadLine());

                Console.Write("State (id): ");
                int state = int.Parse(Console.ReadLine());

                Console.Write("Created at (e.g. dd/mm/yyyy): ");
                DateTime createdAt = DateTime.Parse(Console.ReadLine());

                Console.Write("Finished at (e.g. dd/mm/yyyy): ");
                DateTime finishedAt = DateTime.Parse(Console.ReadLine());

                task = new Models.Task() 
                { 
                    Id = itemId, Description = description, Name = name, ProjectId = projectId,
                    PerformerId = performerId, State = state, CreatedAt = createdAt, FinishedAt = finishedAt 
                };
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }

            return task;
        }

        private Models.Team CreateTeam(params int[] id)
        {
            Models.Team team = null;

            int itemId = 0;

            if (id.Length != 0) itemId = id[0];

            Console.Write("Name: ");
            string name = Console.ReadLine();

            try
            {
                Console.Write("Created at (e.g. dd/mm/yyyy): ");
                DateTime cretedAt = DateTime.Parse(Console.ReadLine());

                team = new Models.Team() { Id = itemId, Name = name, CreatedAt = cretedAt };
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }

            return team;
        }

        private Models.TaskState CreateTaskState(params int[] id)
        {
            int itemId = 0;

            if (id.Length != 0) itemId = id[0];

            Console.Write("Value: ");
            string value = Console.ReadLine();

            return new Models.TaskState() { Id = itemId, Value = value };
        }

        private Models.User CreateUser(params int[] id)
        {
            Models.User user = null;

            int itemId = 0;

            if (id.Length != 0) itemId = id[0];

            Console.Write("First name: ");
            string firstName = Console.ReadLine();

            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            Console.Write("Email: ");
            string email = Console.ReadLine();

            try
            {
                Console.Write("Team id: ");
                int teamId = int.Parse(Console.ReadLine());

                Console.Write("Birthday (e.g. dd/mm/yyyy): ");
                DateTime birthday = DateTime.Parse(Console.ReadLine());

                Console.Write("Registered at (e.g. dd/mm/yyyy): ");
                DateTime registeredAt = DateTime.Parse(Console.ReadLine());

                user = new Models.User() 
                { 
                    Id = itemId, FirstName = firstName, LastName = lastName, Email = email, 
                    TeamId = teamId, Birthday = birthday, RegisteredAt = registeredAt
                };
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }

            return user;
        }
    }
}
